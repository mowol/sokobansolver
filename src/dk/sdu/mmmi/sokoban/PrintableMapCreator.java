package dk.sdu.mmmi.sokoban;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Scanner;

import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.fop.svg.PDFTranscoder;

/**
 * Class that creats an svg file from a map file in sokoban.
 * Requires Apache batik 1.7 with the following files in the classpath: 
 * 		batik-anim.jar
 * 		batik-awt-util.jar
 * 		batik-bridge.jar
 * 		batik-codec.jar
 * 		batik-css.jar
 * 		batik-dom.jar
 * 		batik-ext.jar
 * 		batik-extension.jar
 * 		batik-gui-util.jar
 * 		batik-gvt.jar
 * 		batik-parser.jar
 * 		batik-script.jar
 * 		batik-svg-dom.jar
 * 		batik-svggen.jar
 * 		batik-swing.jar
 * 		batik-transcoder.jar
 * 		batik-util.jar
 * 		batik-xml.jar
 * 		pdf-transcoder.jar
 * 		xml-apis-ext.jar
 * 		xml-apis.jar 
 * @author Morten
 */
public class PrintableMapCreator {

	/**
	 * Main method
	 * @param args map file
	 * @throws Exception any errors
	 */
	public static void main(String[] args) throws Exception {
		//Variables for the map
		final int gridSize = 300;
		final int halfGrid = gridSize / 2;
		final int offSet = gridSize;
		
		//Safety check on args
		if (args.length < 1) {
			System.err.println("Need mapfile as argument");
			System.exit(-1);
		}
		//Load map file and create map array
		char[][] map = getMapArray(args[0]); //returns cols,rows
		
		//Make sure that cols (x) is the smallest dimension and rotate if not
		if (map.length > map[0].length) {
			int w = map.length;
			int h = map[0].length;
			char transposed[][] = new char[h][w];
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j++) {
					transposed[i][j] = map[j][h-i-1];
				}
			}
			map = transposed;
		}
		
		//Safety check (again assuming a border)
		if (map.length - 2 > 5) {
			System.err.println("Cannot deal with maps bigger than 5 in smallest direction");
			System.exit(-1);
		}
		
		//Setup stringbuilder for output
		StringBuilder sb = new StringBuilder();
		
		int height = (map[0].length - 2) * gridSize + 2 * offSet;
		
		//Write out beginning
		sb.append(String.format("<svg width=\"1500mm\" height=\"%dmm\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">\n",
				height));
		
		//Write do not remove text
		sb.append("<text transform=\"rotate(180)\" text-anchor=\"middle\" x=\"-750mm\" y=\"-150mm\" font-family=\"Arial\" font-size=\"100mm\">DO NOT REMOVE!</text>");
		sb.append("<text transform=\"rotate(180)\" text-anchor=\"middle\" x=\"-750mm\" y=\"-50mm\" font-family=\"Arial\" font-size=\"100mm\">mgw@mmmi.sdu.dk</text>");
		
		//Main for loop (which also removes border)
		for (int c = 1; c < map.length-1; c++) {
			for (int r = 1; r < map[0].length-1; r++) {
				//Check if this coordinate has is a walkable field
				if (map[c][r] != 'X') {
					sb.append(String.format("\t<line x1=\"%dmm\" y1=\"%dmm\" x2=\"%dmm\" y2=\"%dmm\" stroke=\"black\" stroke-width=\"14mm\" />\n",
						(c-1)*gridSize,(r-1)*gridSize+halfGrid+offSet,c*gridSize,(r-1)*gridSize+halfGrid+offSet)); //horizontal
					sb.append(String.format("\t<line x1=\"%dmm\" y1=\"%dmm\" x2=\"%dmm\" y2=\"%dmm\" stroke=\"black\" stroke-width=\"14mm\" />\n",
						(c-1)*gridSize+halfGrid,(r-1)*gridSize+offSet,(c-1)*gridSize+halfGrid,r*gridSize+offSet)); //vertical
				}
			}
		}
		
		//Write do not remove text
		sb.append(String.format("<text text-anchor=\"middle\" x=\"750mm\" y=\"%dmm\" font-family=\"Arial\" font-size=\"100mm\">DO NOT REMOVE!</text>",
				height-150));
		sb.append(String.format("<text text-anchor=\"middle\" x=\"750mm\" y=\"%dmm\" font-family=\"Arial\" font-size=\"100mm\">mgw@mmmi.sdu.dk</text>",
				height-50));
				
		//Write out end
		sb.append("</svg>");
		
		//Write out svg
		FileWriter fw = new FileWriter("output.svg");
		fw.append(sb.toString());
		fw.flush();
		fw.close();
		
		//Transform to pdf
        TranscoderInput input_svg_image = new TranscoderInput(new StringReader(sb.toString()));        
        OutputStream pdf_ostream = new FileOutputStream("output.pdf");
        TranscoderOutput output_pdf_file = new TranscoderOutput(pdf_ostream);               
        Transcoder transcoder = new PDFTranscoder();
        transcoder.transcode(input_svg_image, output_pdf_file);
        pdf_ostream.flush();
        pdf_ostream.close(); 
	}
	
	/**
	 * Loads the map file into a char[][] array
	 * @param mapFile the path to the map file
	 * @return the char[][] array of the map
	 */
	public static char[][] getMapArray(String mapFile)
	{
		char[][] mapArray = null;
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(mapFile));
			Scanner confscan = new Scanner(in.readLine());
			int width = confscan.nextInt();
			int height = confscan.nextInt();
			mapArray = new char[width][height];
			StringBuffer sb;
			for (int y = 0; y < height; y++)
			{
				sb = new StringBuffer(in.readLine());
				char tmp;
				for (int x = 0; x < width; x++)
				{
					tmp = sb.charAt(x);
					mapArray[x][y] = tmp;
				}
			}
			in.close();
			confscan.close();
		}
		catch (Exception e)
		{
			System.err.println("Problems with the given file");
			e.printStackTrace();
		}
		return mapArray;
	}

}
