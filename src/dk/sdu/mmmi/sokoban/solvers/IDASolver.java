package dk.sdu.mmmi.sokoban.solvers;

import java.util.ArrayList;
import java.util.List;

import dk.sdu.mmmi.sokoban.Field;
import dk.sdu.mmmi.sokoban.State;

public class IDASolver extends Solver {
	
	private int highestLevel = 0;
	private int IDAcounter = 0;

	@Override
	protected State getNodeToSearch() {
		//Not used for IDA*
		return null;
	}

	@Override
	public int getClosedListCount() {
		return IDAcounter;
	}

	@Override
	protected void solve() throws Exception {
		int threshold = initialState.calculateScoreAStar();
		boolean done = false;
		while (!done) {
			closedList.put(initialState.hashCode(), Boolean.TRUE);
			int result = iterativeDeepeningAStarSearch(initialState, 0, threshold); 
			if (result == Integer.MIN_VALUE) done = true;
			if (!done) {
				threshold = result;
			}
			if (threshold == Integer.MAX_VALUE) {
				throw new Exception("couldn't get a search node");
			}
			closedList.clear();
		}
		run = false;
		setChanged();
		notifyObservers();
	}

	public int iterativeDeepeningAStarSearch(State s, int g, int threshold) {
		IDAcounter++;
		if (g > highestLevel) highestLevel = g;
		int h = s.calculateScoreAStar();
		if (h == 0) {
			currentState = s;
			return Integer.MIN_VALUE;
		}
		int f = h+g;
		if (f > threshold) {
			return f;
		}
		if (isGoalState(s)) {
			currentState = s;
			return Integer.MIN_VALUE;
		}
		int min = Integer.MAX_VALUE;
		
		//find new states
		State stat1 = new State(s); stat1.setParent(s); stat1.player = new Field(stat1.player.x-1, stat1.player.y);
		State stat2 = new State(s); stat2.setParent(s); stat2.player = new Field(stat2.player.x+1, stat2.player.y);
		State stat3 = new State(s); stat3.setParent(s); stat3.player = new Field(stat3.player.x, stat3.player.y-1);
		State stat4 = new State(s); stat4.setParent(s); stat4.player = new Field(stat4.player.x, stat4.player.y+1);
		State[] states = {stat1,stat2,stat3,stat4};
		List<State> newStates = new ArrayList<State>();
		for (State ss : states) {
			if (ss.parentState == null) {
				newStates.add(ss);
				continue;
			}
			Field nextFieldInDirection = Field.getNextFieldInDirection(ss.parentState.player, ss.player);
			//Check if walkable
			if (ss.empties.contains(ss.player)) {
				//just empty field, OK
				newStates.add(ss);
			} else if (ss.jewels.contains(ss.player)) { //or if jewel
				//check if the next field in same direction is available
				if (ss.empties.contains(nextFieldInDirection)) {
					//move fields around in lists
					ss.jewels.remove(ss.player);
					ss.jewels.add(nextFieldInDirection);
					ss.empties.remove(nextFieldInDirection);
					ss.empties.add(ss.player);
					newStates.add(ss);
				}
			}
		}
		
		for (State succ : newStates) {
			if (!closedList.containsKey(succ.hashCode())) {
				int result =  iterativeDeepeningAStarSearch(succ, g+1, threshold);
				if (result == Integer.MIN_VALUE) return result;
				if (result < min) { min = result; }
			}
		}

		return min;
	}

	@Override
	public int getLevel() {
		return highestLevel;
	}

	@Override
	protected void scoreCalculation(State s) {
		return;
	}

}
