package dk.sdu.mmmi.sokoban.solvers;

import java.util.PriorityQueue;
import java.util.Queue;

import dk.sdu.mmmi.sokoban.State;

public class AStarSolver extends Solver
{
	private Queue<StateHashAndScore> openListIdsAStar;
	
	public void reset() {
		super.reset();
		openListIdsAStar = new PriorityQueue<StateHashAndScore>();
		openListIdsAStar.add(new StateHashAndScore(currentState.hashCode()));
	}

	protected State getNodeToSearch() {
		State node = null;
		//There may be duplicates in openListIdsAStar, but this will remove them. remove() and poll() are O(1), so we should be OK.
		while (node == null) {
			node = openList.remove(openListIdsAStar.poll().hash);
		}
		return node;
	}

	@Override
	protected boolean inOpenList(State s) {
		if (openList.containsKey(s.hashCode())) {
			State existingState = openList.get(s.hashCode());
			int stateScore = s.calculateScoreAStar();
			int existingStateScore = existingState.calculateScoreAStar();
			if (stateScore < existingStateScore) {
				openList.remove(s.hashCode());
				return false; //will re-add it with the newer score
			}
			return true;
		}
		return false;
	}

	@Override
	protected void scoreCalculation(State s) {
		StateHashAndScore shs = new StateHashAndScore(s.hashCode());
		shs.score = s.calculateScoreAStar();
		openListIdsAStar.add(shs);
	}

	private class StateHashAndScore implements Comparable<StateHashAndScore> {
		public int hash;
		public int score;
		
		public StateHashAndScore(int hash) {
			this.hash = hash;
		}
		
		public int compareTo(StateHashAndScore o) {
			if (o.score < this.score)
				return 1;
			else if (o.score > this.score)
				return -1;					
			else 
				return 0;
		}
		
		public String toString() {
			return hash + " (" + score + ")";
		}
		
		public int hashCode() {
			String s = hash + ":" + score;
			return s.hashCode();
		}
		
		public boolean equals(Object obj) {
			return hashCode() == obj.hashCode();
		}
	}

	
}
