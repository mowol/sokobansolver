package dk.sdu.mmmi.sokoban.solvers;

import dk.sdu.mmmi.sokoban.State;

public class DFSSolver extends Solver {

	@Override
	protected State getNodeToSearch() {
		return openList.remove(openListIds.removeLast());
	}

	@Override
	protected void scoreCalculation(State s) {
		return;
	}

}
