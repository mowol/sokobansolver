package dk.sdu.mmmi.sokoban.solvers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.AbstractLinkedMap;
import org.apache.commons.collections4.map.LinkedMap;

import dk.sdu.mmmi.sokoban.Field;
import dk.sdu.mmmi.sokoban.GUI;
import dk.sdu.mmmi.sokoban.State;

@SuppressWarnings("javadoc")
public class FastBFS extends Solver {
	
	private Node initialState;
	private Node goalState;	
	private AbstractLinkedMap<Integer, Node> openList;
	private Node currentNode;
	private int mapRows;
	private int mapCols; 
	private int solverLevel = 0;
	private Map<Integer, Boolean> goalPositions;

	@Override
	protected State getNodeToSearch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void scoreCalculation(State s) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void solve() throws Exception {
		run = true;
		while (run) {
			//check for empty openlist
			if (openList.isEmpty()) {
				run = false;
				throw new Exception("Sorry, couldn't find a solution");
			}
			
			//Get new currentstate, which in BFS is based on a FIFO queue 
			currentNode = openList.remove(openList.firstKey());
			
			//Save current level
			this.solverLevel = currentNode.playerMoves;
			
			//Safety check
			if (currentNode == null) {
				throw new Exception("couldn't get a search node");
			}
			
			//check if already checked
			if (closedList.containsKey(currentNode.hashCode())) {
				continue;
			}
			
			//check for goal
			currentNode.map[currentNode.playerPos] = Node.WALKABLE;
			if (currentNode.hashCode() == this.goalState.hashCode()) {
				run = false;
				setChanged();
				notifyObservers();
				break;
			}
			currentNode.map[currentNode.playerPos] = Node.PLAYER;
			
			//find new nodes
			List<Node> newNodes = currentNode.findNewNodes();
			
			//All nodes are valid, check them in lists
			for (Node s : newNodes) {
				if (closedList.containsKey(s.hashCode())) {
					continue;
				}
				if (openList.containsKey(s.hashCode())) {
					continue;
				}
				s.playerMoves = currentNode.playerMoves+1;
				openList.put(s.hashCode(), s);
			}
			
			//move currentState to closedList
			closedList.put(currentNode.hashCode(), new Boolean(true));
		}
	}
	
	@Override
	public int getLevel() {
		return this.solverLevel;
	}
	
	private Node stateToNode(State s) {
		int[] map = new int[mapRows * mapCols];
		int playerPos = 0;
		int field;
		for (int r = 0; r < mapRows; r++) {
			for (int c = 0; c < mapCols; c++) {
				Field f = new Field(c, r);
				//Assume wall
				field = Node.WALL;
				if (s.player.hashCode() == f.hashCode()) {
					field = Node.PLAYER;
					playerPos = c+r*mapCols;
				} else if (s.jewels.contains(f)) {
					field = Node.JEWEL;
				} else if (s.goals.contains(f)) {
					field = Node.GOAL;
				} else if (s.empties.contains(f)) {
					field = Node.WALKABLE;
				}
				map[c+r*mapCols] = field;
			}
		}
		return new Node(map, playerPos);
	}

	@Override
	public void reset() {
		//Update map variables
		mapCols = GUI.smp.getMapArray().length;
		mapRows = GUI.smp.getMapArray()[0].length; 
		//Reset variables
		openList = new LinkedMap<Integer, Node>(5000000);
		closedList = new HashMap<Integer, Boolean>(5000000);
		goalPositions = new HashMap<Integer, Boolean>(10);
		//Create new goal and initial states from the State class
		this.initialState = this.stateToNode(super.initialState);
		//Add the goal positions to the map for quick lookup
		for (Field f : super.initialState.goals) {
			goalPositions.put(f.x+f.y*this.mapCols, new Boolean(true));
		}
		this.goalState = this.stateToNode(super.goalState);
		this.goalState.map[this.goalState.playerPos] = Node.WALKABLE;
		openList.put(this.initialState.hashCode(), this.initialState);
	}
	
	@Override
	public int getOpenListCount()
	{
		return openList.size();
	}
	
	@Override
	public int getClosedListCount()
	{
		return closedList.size();
	}
	
	private State nodeToState(Node n) {
		//Create a state based on the node
		State s = new State();
		int pos;
		if (n.parent != null) {
			s.parentState = nodeToState(n.parent);
		}
		for (int r = 0; r < mapRows; r++) {
			for (int c = 0; c < mapCols; c++) {
				pos = c+r*mapCols;
				if (pos == n.playerPos) {
					s.empties.add(new Field(c,r));
					s.player = new Field(c,r);
				}
				if (n.map[pos] == Node.GOAL) {
					s.goals.add(new Field(c,r));
					s.empties.add(new Field(c,r));
				} else if (n.map[pos] == Node.JEWEL) {
					s.jewels.add(new Field(c,r));
				} else if (n.map[pos] == Node.WALKABLE) {
					s.empties.add(new Field(c,r));
				}
			}
		}
		return s;
	}
	
	@Override
	public State getCurrentState()
	{
		return nodeToState(currentNode);
	}
	
	private class Node {
		private static final int WALL = 0;
		private static final int JEWEL = 1;
		private static final int PLAYER = 2;
		private static final int GOAL = 3;
		private static final int WALKABLE = 4;
		private int[] map;
		private Node parent;
		private int playerPos;
		private int playerMoves = 0;
		
		public Node(int[] map, int playerPos) {
			this.map = map;
			this.playerPos = playerPos;
		}
		
		public Node(Node parent, int[] map, int playerPos) {
			this(map, playerPos);
			this.parent = parent;
		}
		
		@Override
		public int hashCode() {
			return Arrays.hashCode(map);
		}
		
		public List<Node> findNewNodes() {
			//Variables
			List<Node> newNodes = new ArrayList<Node>();
			int newPos;
			//Find new positions
			int[] positionAdds = new int[] {-mapCols, 1, mapCols, -1};
			//Make a loop
			for (int i = 0; i < positionAdds.length; i++) {
				newPos = playerPos+positionAdds[i];
				//Safety check for the new position
				if (newPos < 0 || newPos > map.length) {
					continue;
				}
				//Check for wall
				if (map[newPos] == Node.WALL) {
					continue;
				}
				//Check for empty field
				if (map[newPos] >= Node.GOAL) {
					//Update positions and add node
					int[] newMap = Arrays.copyOf(map, map.length);
					newMap[newPos] = Node.PLAYER;
					if (goalPositions.containsKey(playerPos)) { 
						newMap[playerPos] = Node.GOAL;
					} else {
						newMap[playerPos] = Node.WALKABLE;
					}
					Node n = new Node(this, newMap, newPos);
					newNodes.add(n);
					continue;
				}
				//If we are here we know there is a jewel, so calculate jewels new position
				int newJewelPos = newPos+positionAdds[i];
				//Safety check
				if (newJewelPos < 0 || newJewelPos > map.length) {
					continue;
				}
				//And only check for wall
				if (map[newJewelPos] == Node.WALL) {
					continue;
				}
				//Otherwise ok, lets add it
				int[] newMap = Arrays.copyOf(map, map.length);
				newMap[newJewelPos] = Node.JEWEL;
				newMap[newPos] = Node.PLAYER;
				if (goalPositions.containsKey(playerPos)) {
					newMap[playerPos] = Node.GOAL;
				} else {
					newMap[playerPos] = Node.WALKABLE;
				}
				Node n = new Node(this, newMap, newPos);
				newNodes.add(n);
			}
			return newNodes;
		}
		
//		public List<Node> findNewNodes() {
//			List<Node> newNodes = new ArrayList<Node>();
//			//check up
//			try {
//				int newPos = playerPos-mapCols;
//				if (map[newPos] > Node.WALL) {
//					//No wall, check for jewel
//					if (map[newPos] == Node.JEWEL) {
//						//jewel, check if the next one is ok
//						if (map[newPos-mapCols] <= Node.JEWEL) {
//							//either wall or jewel, dont add
//						} else {
//							//add and move jewel and player
//							int[] newMap = Arrays.copyOf(map, map.length);
//							newMap[newPos-mapCols] = Node.JEWEL;
//							newMap[newPos] = Node.PLAYER;
//							newMap[playerPos] = Node.WALKABLE;
//							Node n = new Node(this, newMap, newPos);
//							newNodes.add(n);
//						}
//					} else {
//						//add and move player
//						int[] newMap = Arrays.copyOf(map, map.length);
//						newMap[newPos] = Node.PLAYER;
//						newMap[playerPos] = Node.WALKABLE;
//						Node n = new Node(this, newMap, newPos);
//						newNodes.add(n);
//					}
//				}
//			} catch (ArrayIndexOutOfBoundsException e) {
//				//do nothing
//			}
//			//check right
//			try {
//				int newPos = playerPos+1;
//				if (map[newPos] > Node.WALL) {
//					//No wall, check for jewel
//					if (map[newPos] == Node.JEWEL) {
//						//jewel, check if the next one is ok
//						if (map[newPos+1] <= Node.JEWEL) {
//							//either wall or jewel, dont add
//						} else {
//							//add and move jewel and player
//							int[] newMap = Arrays.copyOf(map, map.length);
//							newMap[newPos+1] = Node.JEWEL;
//							newMap[newPos] = Node.PLAYER;
//							newMap[playerPos] = Node.WALKABLE;
//							Node n = new Node(this, newMap, newPos);
//							newNodes.add(n);
//						}
//					} else {
//						//add and move player
//						int[] newMap = Arrays.copyOf(map, map.length);
//						newMap[newPos] = Node.PLAYER;
//						newMap[playerPos] = Node.WALKABLE;
//						Node n = new Node(this, newMap, newPos);
//						newNodes.add(n);
//					}
//				}
//			} catch (ArrayIndexOutOfBoundsException e) {
//				//do nothing
//			}
//			//check down
//			try {
//				int newPos = playerPos+mapCols;
//				if (map[newPos] > Node.WALL) {
//					//No wall, check for jewel
//					if (map[newPos] == Node.JEWEL) {
//						//jewel, check if the next one is ok
//						if (map[newPos+mapCols] <= Node.JEWEL) {
//							//either wall or jewel, dont add
//						} else {
//							//add and move jewel and player
//							int[] newMap = Arrays.copyOf(map, map.length);
//							newMap[newPos+mapCols] = Node.JEWEL;
//							newMap[newPos] = Node.PLAYER;
//							newMap[playerPos] = Node.WALKABLE;
//							Node n = new Node(this, newMap, newPos);
//							newNodes.add(n);
//						}
//					} else {
//						//add and move player
//						int[] newMap = Arrays.copyOf(map, map.length);
//						newMap[newPos] = Node.PLAYER;
//						newMap[playerPos] = Node.WALKABLE;
//						Node n = new Node(this, newMap, newPos);
//						newNodes.add(n);
//					}
//				}
//			} catch (ArrayIndexOutOfBoundsException e) {
//				//do nothing
//			}
//			//check left
//			try {
//				int newPos = playerPos-1;
//				if (map[newPos] > Node.WALL) {
//					//No wall, check for jewel
//					if (map[newPos] == Node.JEWEL) {
//						//jewel, check if the next one is ok
//						if (map[newPos-1] <= Node.JEWEL) {
//							//either wall or jewel, dont add
//						} else {
//							//add and move jewel and player
//							int[] newMap = Arrays.copyOf(map, map.length);
//							newMap[newPos-1] = Node.JEWEL;
//							newMap[newPos] = Node.PLAYER;
//							newMap[playerPos] = Node.WALKABLE;
//							Node n = new Node(this, newMap, newPos);
//							newNodes.add(n);
//						}
//					} else {
//						//add and move player
//						int[] newMap = Arrays.copyOf(map, map.length);
//						newMap[newPos] = Node.PLAYER;
//						newMap[playerPos] = Node.WALKABLE;
//						Node n = new Node(this, newMap, newPos);
//						newNodes.add(n);
//					}
//				}
//			} catch (ArrayIndexOutOfBoundsException e) {
//				//do nothing
//			}
//			return newNodes;
//		}
	}
}
