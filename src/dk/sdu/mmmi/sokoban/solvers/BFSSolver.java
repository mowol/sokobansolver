package dk.sdu.mmmi.sokoban.solvers;

import dk.sdu.mmmi.sokoban.State;

public class BFSSolver extends Solver {

	@Override
	protected State getNodeToSearch() {
		return openList.remove(openListIds.removeFirst());
	}

	@Override
	protected void scoreCalculation(State s) {
		return;
	}
	
	

}
