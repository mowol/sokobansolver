package dk.sdu.mmmi.sokoban.solvers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import dk.sdu.mmmi.sokoban.Field;
import dk.sdu.mmmi.sokoban.State;

public abstract class Solver extends Observable implements Runnable {
	
	protected State currentState;
	protected State goalState;
	protected State initialState;
	protected boolean run = false;
	protected Map<Integer, State> openList;
	protected Map<Integer, Boolean> closedList;
	protected LinkedList<Integer> openListIds;
	
	/**
	 * The algorithm specific method that returns the next node to search
	 * @return the next node (State) to search
	 */
	protected abstract State getNodeToSearch();
	/**
	 * Algorithm specific score calculation if required
	 * @param the current State that has been found and that the score should be calculated upon.
	 */
	protected abstract void scoreCalculation(State s);
	
	/**
	 * Returns the depth the current algorithm is at. This is equal to the amount of moves the player has made
	 * @return the amount of moves the player has made
	 */
	public int getLevel() {
		return currentState.playerMoves;
	}
	
	/**
	 * Gives the amount of visited nodes
	 * @return the amount of visited nodes
	 */
	public int getClosedListCount() {
		return closedList.size();
	}
	
	/**
	 * Sets the inital state for the solver
	 * @param The initial state
	 */
	public void setInitialState(State state) {
		this.initialState = state;
	}

	/**
	 * Sets the goal state for the solver
	 * @param The goal state
	 */
	public void setGoalState(State state) {
		this.goalState = state;
	}
	
	/**
	 * The solving algorithm. Throws exception on failures and incompletions
	 * @throws Exception
	 */
	protected void solve() throws Exception {
		run = true;
		while (run) {
			//check for empty openlist
			if (openList.isEmpty()) {
				run = false;
				throw new Exception("Sorry, couldn't find a solution");
			}
			
			//get new currentstate (algorithm specific, so in a method)
			currentState = getNodeToSearch(); 
			
			//Safety check
			if (currentState == null) {
				throw new Exception("couldn't get a search node");
			}
				
			
			//check if already checked
			if (closedList.containsKey(currentState.hashCode())) {
				continue;
			}
			
			//check for goal
			if (isGoalState(currentState)) {
				run = false;
				setChanged();
				notifyObservers();
				break;
			}
			
			//find new states
			State stat1 = new State(currentState); stat1.setParent(currentState); stat1.player = new Field(stat1.player.x-1, stat1.player.y);
			State stat2 = new State(currentState); stat2.setParent(currentState); stat2.player = new Field(stat2.player.x+1, stat2.player.y);
			State stat3 = new State(currentState); stat3.setParent(currentState); stat3.player = new Field(stat3.player.x, stat3.player.y-1);
			State stat4 = new State(currentState); stat4.setParent(currentState); stat4.player = new Field(stat4.player.x, stat4.player.y+1);
			State[] states = {stat1,stat2,stat3,stat4};
			List<State> newStates = fixAndReturn(states);
			
			//states ok, add new states to openList
			for (State s : newStates) {
				s.playerMoves = currentState.playerMoves + 1;
				openList.put(s.hashCode(), s);
				
				//Calculate score if algorithm requires
				scoreCalculation(s);
				
				//Add to open list.
				openListIds.add(s.hashCode());
			}
			
			//move currentState to closedList
			closedList.put(currentState.hashCode(), true);
		}
	}
	
	/**
	 * Resets the solver so its ready again
	 */
	public void reset() {
		currentState = initialState;
		openList = new HashMap<Integer, State>(5000000);
		openList = Collections.synchronizedMap(openList);
		closedList = new HashMap<Integer, Boolean>(5000000);
		closedList = Collections.synchronizedMap(closedList);
		openList.put(currentState.hashCode(), currentState);
		openListIds = new LinkedList<Integer>();
		openListIds.add(currentState.hashCode());
	}
	
	/**
	 * Thread runner, calls solve()
	 */
	@Override
	public void run() {
		try {
			solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the amount of nodes in the open list
	 * @return The amount of nodes in the open list
	 */
	public int getOpenListCount()
	{
		return openList.size();
	}
	
	/**
	 * Returns whether the solver is currently in the process of solving a puzzle
	 * @return true for running, false for not running
	 */
	public boolean isRunning() {
		return run;
	}
	
	/**
	 * Returns the current state that the solver is visiting. This state will be the solution if the solver is complete
	 * @return the solver's current state
	 */
	public State getCurrentState()
	{
		return currentState;
	}
	
	/**
	 * Stops the solver
	 */
	public void terminate() {
		run = false;
	}
	
	/**
	 * Check if the state s is the goal state
	 * @param s the state to check
	 * @return true for goal state, false otherwise
	 */
	protected boolean isGoalState(State s) {
		return goalState.jewels.containsAll(s.jewels);
	}
	
	/**
	 * Fixes any states gives by checking if they are possible states and also check if the states are in the open and closed lists
	 * @param states to check
	 * @return a List of states that are fine to add to the openList
	 */
	protected List<State> fixAndReturn(State[] states) {
		List<State> newStates = new ArrayList<State>();
		for (State s : states) {
			if (s.parentState == null) {
				newStates.add(s);
				continue;
			}
			Field nextFieldInDirection = Field.getNextFieldInDirection(s.parentState.player, s.player);
			//Check if walkable
			if (s.empties.contains(s.player)) {
				//just empty field, OK
				if (notExistsInLists(s)) {
					newStates.add(s);
				}
			} else if (s.jewels.contains(s.player)) { //or if jewel
				//check if the next field in same direction is available
				if (s.empties.contains(nextFieldInDirection)) {
					//move fields around in lists
					s.jewels.remove(s.player);
					s.jewels.add(nextFieldInDirection);
					s.empties.remove(nextFieldInDirection);
					s.empties.add(s.player);
					if (notExistsInLists(s)) {
						newStates.add(s);
					}
				}
			}
		}
		return newStates;
	}
	
	/**
	 * Checks if the given state is not in any of the closed or open lists
	 * @param s the state to check
	 * @return true for ok to add, false for already in lists
	 */
	protected boolean notExistsInLists(State s) {
		if (!inClosedList(s) && !inOpenList(s)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the given state is in the closed list
	 * @param s the state to check
	 * @return true if in list, false otherwise
	 */
	protected boolean inClosedList(State s) {
		if (closedList.containsKey(s.hashCode()))
			return true;
		return false;
	}
	
	/**
	 * Checks if the given state is in the open list
	 * @param s the state to check
	 * @return true if in list, false otherwise
	 */
	protected boolean inOpenList(State s) {
		if (openList.containsKey(s.hashCode())) {
			return true;
		}
		return false;
	}
}
