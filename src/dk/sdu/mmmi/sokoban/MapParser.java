package dk.sdu.mmmi.sokoban;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class MapParser
{
	private static String defaultMap = "maps/default.txt";
	private State initialState;
	private char[][] mapArray;

	public MapParser() {
		this(defaultMap);
	}
	
	public MapParser(String mapFile) {
		parseMap(mapFile);
	}
	
	public State getInitialState() {
		return initialState;
	}
	
	public State getGoalState() {
		State goalState = new State(initialState);
		goalState.empties.addAll(initialState.jewels);
		goalState.jewels.clear();
		goalState.jewels.addAll(initialState.goals);
		return goalState;
	}
	
	private void parseMap(String map)
	{
		initialState = new State();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(map));
			Scanner confscan = new Scanner(in.readLine());
			int width = confscan.nextInt();
			int height = confscan.nextInt();
			mapArray = new char[width][height];
			StringBuffer sb;
			for (int y = 0; y < height; y++)
			{
				sb = new StringBuffer(in.readLine());
				char tmp;
				for (int x = 0; x < width; x++)
				{
					tmp = sb.charAt(x);
					mapArray[x][y] = tmp;
					switch (tmp)
					{
					case '.':
						initialState.empties.add(new Field(x,y));
						break;
					case 'J':
						initialState.jewels.add(new Field(x,y));
						break;
					case 'G':
						initialState.empties.add(new Field(x,y));
						initialState.goals.add(new Field(x,y));
						break;
					case 'M':
						initialState.player = new Field(x,y);
						initialState.empties.add(new Field(x,y));
						break;
					default:
						break;
					}
				}
			}
			in.close();
			confscan.close();
		}
		catch (Exception e)
		{
			System.err.println("Problems with the given file");
			e.printStackTrace();
		}
	}

	public char[][] getMapArray()
	{
		return mapArray;
	}
}
