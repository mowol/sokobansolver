package dk.sdu.mmmi.sokoban;

import java.util.HashSet;
import java.util.Set;

public class State implements Comparable<State>
{
	public State parentState;
	public Set<Field> goals;
	public Set<Field> jewels;
	public Set<Field> empties;
	public Field player;
	public int score = 0; //heuristic
	public int playerMoves = 0;
	private int hashcode = Integer.MIN_VALUE;

	public State()
	{
		this.goals = new HashSet<Field>();
		this.jewels = new HashSet<Field>();
		this.empties = new HashSet<Field>();
	}

	public State(State parent, Set<Field> goals, Set<Field> jewels, Set<Field> empties, Field player, int score)
	{
		this.parentState = parent;
		this.goals = goals;
		this.jewels = jewels;
		this.empties = empties;
		this.player = player;
		this.score = score;
	}

	//Copies the state deeply, instead of creating referencing copies of lists
	public State(State copyFrom) {
		this(copyFrom.parentState, copyFrom.goals, new HashSet<Field>(copyFrom.jewels), 
				new HashSet<Field>(copyFrom.empties), new Field(copyFrom.player), copyFrom.score);
	}
	
	/**
	 * Sets the parent state
	 * @param parent the state that should be parent to the this state
	 */
	public void setParent(State parent) {
		this.parentState = parent;
	}

	/**
	 * Calculates a unique hash for this state TODO: Use zobrist hash
	 * @return the hash
	 */
	public int hashCode() {
		if (this.hashcode != Integer.MIN_VALUE) {
			return this.hashcode;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(player.hashCode());
		for (Field f : jewels)
			sb.append(f.hashCode());
		this.hashcode = sb.toString().hashCode();
		return this.hashcode;
	}

	@Override
	public boolean equals(Object obj) {
		return hashCode() == obj.hashCode();
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Score: " + score);
		sb.append("Player: " + player);
		sb.append("Jewels: " + jewels.toString());
		return sb.toString();
	}

	@Override
	public int compareTo(State arg0)
	{
		if (this.score < arg0.score) return -1;
		else if (this.score > arg0.score) return 1;
		else return 0;
	}

	/**
	 * Heuristic function for A*
	 * @return the score
	 */
	public int calculateScoreAStar() {
		double score = 0;
		//Score should include the amount of moves the player has made
		score += 2.0 * playerMoves;
		//And also the combined distance from all jewels to all goals
		score += calculateScoreIDAStar();
		//And also add the combined distance to all jewels from player
		for(Field jewel : jewels)
		{
			if(goals.contains(jewel))
			{
				continue;
			}
			score += Math.abs(jewel.x - player.x) + Math.abs(jewel.y - player.y);
		}
		return (int) score;
	}
	
	/**
	 * Heuristic function for IDA*
	 * @return the score
	 */
	public int calculateScoreIDAStar() {
		double score = 0;
		//And also the combined distance from all jewels to all goals
		for(Field jewel : jewels)
		{
			int minimumDistanceToGoal = Integer.MAX_VALUE;
			if(goals.contains(jewel))
			{
				continue;
			}
			for(Field goal : goals)
			{
				int distance = Math.abs(jewel.x - goal.x) + Math.abs(jewel.y - goal.y);
				if(distance < minimumDistanceToGoal)
				{
					minimumDistanceToGoal = distance;
				}
			}
			score += minimumDistanceToGoal;
		}
		return (int) score;
	}

	/**
	 * Returns the player movement between two states
	 * @param oldState the from state
	 * @param s the to state
	 * @return 0 for up, 1 for right, 2 for down, 3 for left. If a diamond is moved, 10 is added to the result
	 */
	public static int getPlayerMovement(State oldState, State s) {
		//Get the two player positions
		Field oldStatePos = oldState.player;
		Field statePos = s.player;
		//Get the direction
		int direction;
		if (oldStatePos.y > statePos.y) {
			direction = 0; //up
		} else if (oldStatePos.y < statePos.y) {
			direction = 2; //down
		} else {
			if (oldStatePos.x < statePos.x) {
				direction = 1; //right 
			} else {
				direction = 3; //left
			}
		}
		//Check if the new position has changed diamond positions
		if (oldState.jewels.hashCode() != s.jewels.hashCode()) {
			direction += 10; //we add 10 to state that a diamond has been moved
		}
		
		return direction;
	}
}
