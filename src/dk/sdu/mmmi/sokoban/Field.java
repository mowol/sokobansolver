package dk.sdu.mmmi.sokoban;

public class Field
{
	public final int x;
	public final int y;
	private int hashcode = Integer.MIN_VALUE;
	
	public Field(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Field(Field f) {
		this.x = f.x;
		this.y = f.y;
	}
	
	@Override
	public int hashCode()
	{
		if (this.hashcode != Integer.MIN_VALUE) {
			return this.hashcode;
		}
		final int prime = 31;
		this.hashcode  = 1;
		this.hashcode = prime * this.hashcode + x;
		this.hashcode = prime * this.hashcode + y;
		return this.hashcode;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Field other = (Field) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
	
	public static Field getNextFieldInDirection(Field oldPlayer, Field newPlayer) {
		int deltax = newPlayer.x - oldPlayer.x;
		int deltay = newPlayer.y - oldPlayer.y;
		return new Field(newPlayer.x + deltax, newPlayer.y + deltay);
	}
}
