package dk.sdu.mmmi.sokoban;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import dk.sdu.mmmi.sokoban.solvers.AStarSolver;
import dk.sdu.mmmi.sokoban.solvers.BFSSolver;
import dk.sdu.mmmi.sokoban.solvers.DFSSolver;
import dk.sdu.mmmi.sokoban.solvers.FastBFS;
import dk.sdu.mmmi.sokoban.solvers.IDASolver;
import dk.sdu.mmmi.sokoban.solvers.Solver;

public class GUI implements Observer
{

	private JFrame frame;
	private GridPane panelMap;
	private JLabel lblOpenNodes;
	private JLabel lblClosedNodes;
	private JLabel lblLevel;
	private Solver solver;
	private MapParser mp;
	private JButton btnToStart;
	private JButton btnStepBack;
	private JButton btnStep;
	private JButton btnToEnd;
	private JButton btnPlay;
	private JLabel lblNodesPrSec;
	private JComboBox<String> comboBoxAlgorithm;
	private JComboBox<String> comboBoxMap;
	private ArrayList<State> states = new ArrayList<State>();
	private int stateidx = 0;
	private long timeStart;
	private Timer updateTimer;
	private Thread solverThread;
	private Timer playTimer;
	public static MapParser smp;


	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run()
			{
				try
				{
					GUI window = new GUI();
					window.frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI()
	{
		mp = new MapParser("maps/2011competition.txt");
		this.smp = mp;
		setAlgorithm(0);
		initializeGUI();
		initializeMap(mp.getMapArray()[0].length, mp.getMapArray().length);
		states.add(mp.getInitialState());
		drawState(0);
	}
	
	private void setAlgorithm(int idx) {
		if (solver != null) {
			solver.deleteObserver(this);
		}
		switch (idx) {
		default:
		case 0:
			solver = new FastBFS();
			break;
		case 1: 
			solver = new BFSSolver();
			break;
		case 2:
			solver = new DFSSolver();
			break;
		case 3:
			solver = new AStarSolver();
			break;
		case 4:
			solver = new IDASolver();
			break;
		}
		solver.setInitialState(mp.getInitialState());
		solver.setGoalState(mp.getGoalState());
		solver.addObserver(this);
		solver.reset();
	}
	
	@Override
	public void update(Observable o, Object arg)
	{
		states.clear();
		//build array of states
		State s = solver.getCurrentState();
		states.add(s);
		while (s.parentState != null) {
			s = s.parentState;
			states.add(0, s);
		}
		
		//activate controls
		btnToStart.setVisible(true);
		btnStepBack.setVisible(true);
		btnPlay.setVisible(true);
		btnStep.setVisible(true);
		btnToEnd.setVisible(true);
		//stop threads
		solver.terminate();
		updateTimer.cancel();
		//Draw state 0
		drawState(0);
		updateInfo();
		frame.getContentPane().setBackground(Color.GREEN);
	}
	
	private void drawState(int idx)
	{
		if (idx >= 0 && idx < states.size()) {
			stateidx = idx;
			updateMap(mp.getMapArray(), states.get(idx));
		}
	}

	private void updateMap(char[][] mapArray, State state)
	{
		//transfer state to mapArray
		for (int x = 0; x < mapArray.length; x++) {
			for (int y = 0; y < mapArray[0].length; y++) {
				if (state.empties.contains(new Field(x,y))) {
					mapArray[x][y] = '.';
				}
				if (state.goals.contains(new Field(x,y))) {
					mapArray[x][y] = 'G';
				}
				if (state.player.equals(new Field(x,y))) {
					mapArray[x][y] = 'M';
				}
				if (state.jewels.contains(new Field(x,y))) {
					mapArray[x][y] = 'J';
				}
			}
		}
		//send updated mapArray to GridPane
		panelMap.updateCells(mapArray);
	}
	
	private void updateInfo() {
		lblOpenNodes.setText(Integer.toString(solver.getOpenListCount()));
		lblClosedNodes.setText(Integer.toString(solver.getClosedListCount()));
		lblLevel.setText(Integer.toString(solver.getLevel()));
		
		long milliseconds = (int) (System.currentTimeMillis() - timeStart);
		long seconds = milliseconds / 1000;
		
		int nodes = (int) (solver.getClosedListCount() / (double) seconds);
		lblNodesPrSec.setText(Integer.toString(nodes));
						
		frame.setTitle(String.format("%2d", seconds / 60) + ":" + String.format("%02d", seconds % 60) + "." + String.format("%3d", milliseconds % 1000));
//		updateMap(mp.getMapArray(), solver.getCurrentState());
	}

	private void initializeGUI()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 620);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JButton btnStartCalculation = new JButton("Start calculation");
		btnStartCalculation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnStartCalculation.getText().toLowerCase().equals("reset")) {
					//Fix gui
					btnStartCalculation.setText("Start calculation");
					frame.getContentPane().setBackground(new Color(238, 238, 238));
					
					//Stop everything
					updateTimer.cancel();
					solver.terminate();
					
					//Update gui
					updateInfo();
					
					//Reset solver
					solver.reset();
				} else {
					//Fix gui
					btnStartCalculation.setText("Reset");
					frame.getContentPane().setBackground(Color.RED);
					
					//Start everything
					Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
					timeStart = System.currentTimeMillis();
					updateTimer = new Timer();
					updateTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									updateInfo();
								}
							});
						}
					}, 0, 500);
					solverThread = new Thread(solver);
					solverThread.setPriority(Thread.MIN_PRIORITY);
					solverThread.start();
				}
			}
		});
		btnStartCalculation.setBounds(6, 6, 146, 29);
		frame.getContentPane().add(btnStartCalculation);
		
		JLabel lblOpenNodesText = new JLabel("Open nodes:");
		lblOpenNodesText.setBounds(164, 11, 86, 16);
		frame.getContentPane().add(lblOpenNodesText);
		
		JLabel lblClosedNodesText = new JLabel("Closed nodes:");
		lblClosedNodesText.setBounds(164, 35, 99, 16);
		frame.getContentPane().add(lblClosedNodesText);
		
		lblOpenNodes = new JLabel("0");
		lblOpenNodes.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblOpenNodes.setBounds(262, 11, 150, 16);
		frame.getContentPane().add(lblOpenNodes);
		
		lblClosedNodes = new JLabel("0");
		lblClosedNodes.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblClosedNodes.setBounds(262, 35, 110, 16);
		frame.getContentPane().add(lblClosedNodes);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(16, 80, 578, 12);
		frame.getContentPane().add(separator);
		
		JLabel lblLevelText = new JLabel("Level: ");
		lblLevelText.setBounds(491, 6, 45, 14);
		frame.getContentPane().add(lblLevelText);
		
		lblLevel = new JLabel("0");
		lblLevel.setFont(new Font("Dialog", Font.BOLD, 13));
		lblLevel.setBounds(548, 6, 46, 14);
		frame.getContentPane().add(lblLevel);
		
		btnToStart = new JButton("<<");
		btnToStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				drawState(0);
			}
		});
		btnToStart.setBounds(366, 24, 35, 29);
		btnToStart.setVisible(false);
		frame.getContentPane().add(btnToStart);
		
		btnStepBack = new JButton("<");
		btnStepBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawState(stateidx-1);
			}
		});
			
		btnStepBack.setBounds(414, 24, 35, 29);
		btnStepBack.setVisible(false);
		frame.getContentPane().add(btnStepBack);
		

		btnPlay = new JButton("P");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playTimer = new Timer();
				playTimer.schedule(new TimerTask() {
					public void run() {
						if (stateidx == states.size() - 1) {
							playTimer.cancel();
						}
						drawState(stateidx+1);
					}
				}, 0, 200);
			}
		});
		btnPlay.setBounds(462, 24, 35, 29);
		btnPlay.setVisible(false);
		frame.getContentPane().add(btnPlay);
		
		btnStep = new JButton(">");
		btnStep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawState(stateidx+1);
			}
		});
		btnStep.setBounds(510, 24, 35, 29);
		btnStep.setVisible(false);
		frame.getContentPane().add(btnStep);
		
		btnToEnd = new JButton(">>");
		btnToEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				drawState(states.size()-1);
			}
		});
		btnToEnd.setBounds(558, 24, 35, 29);
		btnToEnd.setVisible(false);
		frame.getContentPane().add(btnToEnd);
		
		JLabel lblNodesPrSecText = new JLabel("Nodes pr sec:");
		lblNodesPrSecText.setBounds(16, 35, 86, 16);
		frame.getContentPane().add(lblNodesPrSecText);
		
		lblNodesPrSec = new JLabel("0");
		lblNodesPrSec.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblNodesPrSec.setBounds(114, 35, 100, 16);
		frame.getContentPane().add(lblNodesPrSec);
		
		comboBoxAlgorithm = new JComboBox<String>();
		comboBoxAlgorithm.setModel(new DefaultComboBoxModel<String>(new String[] {"FBFS","BFS", "DFS", "A*", "IDA*"}));
		comboBoxAlgorithm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setAlgorithm(comboBoxAlgorithm.getSelectedIndex());
			}
		});
		comboBoxAlgorithm.setBounds(16, 53, 86, 27);
		frame.getContentPane().add(comboBoxAlgorithm);
		
		comboBoxMap = new JComboBox<String>();
		//Find every map in the maps folder
		File folder = new File("maps");
		File[] maps = folder.listFiles();
		String[] mapsNames = new String[maps.length];
		for (int i = 0; i < maps.length; i++) {
			mapsNames[i] = maps[i].getName();
		}
		comboBoxMap.setModel(new DefaultComboBoxModel<String>(mapsNames));
		comboBoxMap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!solver.isRunning()) {
					mp = new MapParser("maps/" + comboBoxMap.getSelectedItem());
					GUI.smp = mp;
					initializeMap(mp.getMapArray()[0].length, mp.getMapArray().length);
					states.clear();
					states.add(mp.getInitialState());
					solver.setInitialState(mp.getInitialState());
					solver.setGoalState(mp.getGoalState());
					solver.reset();
					drawState(0);
				}
			}
		});
		comboBoxMap.setBounds(124, 53, 200, 27);
		frame.getContentPane().add(comboBoxMap);
		
		JButton btnNewButton = new JButton("Get path");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, new JTextArea(calculatePath()), "Robot path", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnNewButton.setBounds(332, 52, 117, 29);
		frame.getContentPane().add(btnNewButton);
	}
	
	private String getMovement(State oldState, State s) {
		//Get movement
		int movement = State.getPlayerMovement(oldState, s);
		//Check if movement is more than 10
		boolean diamondMoved = false;
		if (movement >= 10) {
			diamondMoved = true;
			movement -= 10;
		}
		switch (movement) {
		case 0:
			return diamondMoved ? "U" : "u";
		case 1:
			return diamondMoved ? "R" : "r";
		case 2:
			 return diamondMoved ? "D" : "d";
		case 3:
			return diamondMoved ? "L" : "l";
		}
		return "";
	}
	
	private String calculatePath() {
		StringBuilder sb = new StringBuilder();
		State oldState = null;
		for (State s : states) {
			if (oldState == null) {
				oldState = s;
				continue;
			}
			sb.append(getMovement(oldState, s));
			oldState = s;
		}
		return sb.toString();
	}

	private void initializeMap(int rows, int cols) {
		if (panelMap != null)
			frame.getContentPane().remove(panelMap);
		panelMap = new GridPane(rows, cols);
		panelMap.setBounds(26, 92, 501, 501);
		frame.getContentPane().add(panelMap);
	}
	
	@SuppressWarnings("serial")
	private class GridPane extends JPanel {

        private int columnCount;
        private int rowCount;
        private int cellWidth;
        private int cellHeight;
        private int xOffset;
        private int yOffset;
        char[][] mapArray;

        public GridPane(int rows, int cols)
		{
        	int width = 500;
            int height = 500;
            
            columnCount = cols;
            rowCount = rows;

            cellWidth = width / columnCount;
            cellHeight = height / rowCount;
            
            if (cellWidth < cellHeight) {
            	cellHeight = cellWidth;
            } else {
            	cellWidth = cellHeight;
            }

            xOffset = (width - (columnCount * cellWidth)) / 2;
            yOffset = (height - (rowCount * cellHeight)) / 2;
		}

		public void updateCells(char[][] mapArray)
		{
        	this.mapArray = mapArray; 
        	repaint();
		}

		@Override
        public Dimension getPreferredSize() {
            return new Dimension(200, 200);
        }

        @Override
        public void invalidate() {
            super.invalidate();
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            
            for (int row = 0; row < rowCount; row++) {
                for (int col = 0; col < columnCount; col++) {
                    Rectangle cell = new Rectangle(xOffset + (col * cellWidth), yOffset + (row * cellHeight), cellWidth, cellHeight);
                    //set color
                    Color color = Color.WHITE;
                    switch(mapArray[col][row]) {
                    case 'M':
                    	color = Color.BLUE;
                    	break;
                    case 'X':
                    	color = Color.BLACK;
                    	break;
                    case 'G':
                    	color = Color.GREEN;
                    	break;
                    case 'J':
                    	color = Color.RED;
                    	break;
                    }
                    g2d.setColor(color);
                    g2d.fill(cell);;
                    color = Color.GRAY;
                    g2d.setColor(color);
                    g2d.draw(cell);
                }
            }

            g2d.dispose();
        }
    }
}
